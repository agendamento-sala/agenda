import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { Agenda } from './agenda';
import { AppService } from './app.service';
import { stringify } from 'querystring';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [AppService]
})
export class AppComponent implements OnInit {

    agendas: Agenda[];
    novaAgenda: Agenda;
    agendaForm: FormGroup;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private appService: AppService
    ) { }

    ngOnInit(): void {
        this.agendaForm = this.formBuilder.group({
            sala: ['', Validators.required],
            dataInicioAgenda: ['', Validators.required],
            dataFimAgenda: ['', Validators.required],
            tituloAgenda: ['', Validators.required],
            organizador: ['', Validators.required]
        });

        this.atualizarLista();
    }

    atualizarLista() {
        this.novaAgenda = new Agenda();
        this.appService.getListaAgenda().then((agendas: Agenda[]) => this.agendas = agendas);
    }

    get f() {
        return this.agendaForm.controls;
    }


    salvarAgenda() {
       /*
        if (this.agendaForm.valid && !this.agendaForm.pending) {
            const sala = this.agendaForm.get('sala').value;
            const dataHora = this.agendaForm.get('dataHora').value;
            const organizador = this.agendaForm.get('organizador').value;
            const tituloAgenda = this.agendaForm.get('tituloAgenda').value;
            const descricao = this.agendaForm.get('descricao').value;
        }
        */

        this.appService.salvarAgenda(this.novaAgenda)
            .then(
                (a) => this.atualizarLista()
            )
            .catch(error => {
                console.log(error);
            });

    }

    apagarAgenda(id: String) {
        this.appService.apagarAgenda(id).then((id) => this.atualizarLista());
    }

    buscarAgenda(id: String) {
        this.appService.buscarAgenda(id).then((a) => this.novaAgenda = a);
    }

}
