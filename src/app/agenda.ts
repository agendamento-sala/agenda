export class Agenda {
    _id?: string;
    titulo: string;
    dataInicio: Date;
    dataFim: Date;
    sala: string;
    organizador: string;
}
