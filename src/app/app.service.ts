import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Agenda } from './agenda';

@Injectable()
export class AppService {

  private agendaUrl = 'http://localhost:8080/api/agenda';

  constructor(private http: Http) { }

  getListaAgenda(): Promise<Agenda[]> {
    return this.http.get(this.agendaUrl).toPromise().then(
      response => response.json() as Agenda[]
    );
  }

  salvarAgenda(agenda: Agenda): Promise<Agenda> {
    return this.http.post(this.agendaUrl, agenda).toPromise().then(
      response => response.json() as Agenda
    );
  }

  apagarAgenda(id: String): Promise<String> {
    return this.http.delete(`${this.agendaUrl}/${id}`).toPromise().then(
      response => response.json() as String
    );
  }

  buscarAgenda(id: String): Promise<Agenda> {
    return this.http.get(`${this.agendaUrl}/${id}`).toPromise().then(
      response => response.json() as Agenda
    );
  }

}
