const express = require('express')
const bodyParser = require('body-parser')
const model = require('./model')


function validar(pAgenda) {

  v_mensagem = "";
  console.log(pAgenda);

  if (pAgenda.dataInicio == null) {
    v_mensagem = '[dataInicio] deve ser preenchida!';
  }
  else if (pAgenda.dataFim == null) {
    v_mensagem = '[dataFim] deve ser preenchida!';
  }
  else if (pAgenda.titulo == null) {
    v_mensagem = '[titulo] deve ser preenchido!';
  }
  else if (pAgenda.organizador == null) {
    v_mensagem = '[organizador] deve ser preenchida!';
  }
  else if (pAgenda.sala == null) {
    v_mensagem = '[sala] deve ser preenchida!';
  }

  return v_mensagem;
}


const listar = (req, res) =>
  model.Agenda.find({}, (err, agendas) => res.send(agendas))

const salvar = (req, res) => {

  v_mensagem = validar(new model.Agenda(req.body));

  v_inicio = req.body.dataInicio;
  v_fim = req.body.dataFim;
  v_sala = req.body.sala;

  if (v_mensagem == "") {
    model.Agenda.find({ $or: [{ dataInicio: { $gte: v_inicio, $lte: v_fim } }, { dataFim: { $gte: v_inicio, $lte: v_fim } }], sala: v_sala }, (err, result) => {
      if (result == "") {
        if (!req.body._id) {
          new model.Agenda(req.body).save((err) => res.status(201).send(req.body))
        }
        else {
          model.Agenda.findOneAndUpdate(
            { _id: req.body._id }, req.body, { upsert: true },
            (err, cliente) => {
              if (!err) res.status(200).send(req.body)
              else res.status(500).send({ message: err })
            }
          )
        }
      }
      else {
        res.status(400).send({ message: 'Já existe um agendamento para a ' + v_sala + ' no horário ' + v_inicio + ' até às ' + v_fim });
      }
    });
  }
  else {
    res.status(400).send({ message: v_mensagem });
  }
}

const buscar = (req, res) =>
  model.Agenda.findOne({ _id: req.params.id },
    (err, cliente) => res.status(cliente ? 200 : 404).send(cliente)
  )

const apagar = (req, res) =>
  model.Agenda.deleteOne({ _id: req.params.id },
    (err) => res.json(req.params.id)
  )

module.exports = express()
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: true }))
  .use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
  })
  .get('/agenda', listar)
  .get('/agenda/:id', buscar)
  .post('/agenda', salvar)
  .delete('/agenda/:id', apagar)
