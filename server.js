const express = require('express')
const app = express()
//.use(express.static('dist'))
.use('/api', require('./rest'))
require('http').createServer(app).listen(8080, () =>
  console.log(`Aplicativo iniciado na porta ${8080}...`)
)
