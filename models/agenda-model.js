'use strict'

const mongoose = require('mongoose');
const schema = mongoose.Schema;

const agendaModel = new schema({
    data: Date,
    organizador: String,
    titulo: String,
    descricao: String,
    duracao: Number,
    sala: String
}, { versionKey: false });

module.exports = mongoose.model('Agenda', agendaModel);