const mongoose = require('mongoose')

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true })

module.exports = {
  Agenda: mongoose.model('Agenda2', new mongoose.Schema({
    titulo: String,
    dataInicio: Date,
    dataFim: Date,
    sala: String,
    organizador: String
  }, { versionKey: false }))
}
