const Agenda = require('./models/agenda-model');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true })


const api = express();


var v_agenda_valida = true;

api.use(bodyParser.json());

api.get('/agenda', (req, res) => Agenda.find({}, (err, agendas) => res.send(agendas)));

api.post('/agenda', (req, res) => {
  let agenda = new Agenda
  
  agenda.data = req.body.data
  agenda.organizador = req.body.organizador;
  agenda.titulo = req.body.titulo
  agenda.descricao = req.body.descricao
  agenda.duracao = req.body.duracao
  agenda.sala = req.body.sala

  if(agenda.data == null) {
    v_agenda_valida = false;    
    res.status(200).send({ message: 'Data deve ser preenchida!' });
  }
  else if(agenda.organizador == null) {
    v_agenda_valida = false;    
    res.status(200).send({ message: 'Organizador deve ser preenchido!' });
  }
  else if(agenda.titulo == null) {
    v_agenda_valida = false;    
    res.status(200).send({ message: 'Título deve ser preenchido!' });
  }
  else if(agenda.descricao == null) {
    v_agenda_valida = false;    
    res.status(200).send({ message: 'Descrição deve ser preenchida!' });
  }
  else if(agenda.sala == null) {
    v_agenda_valida = false;    
    res.status(200).send({ message: 'Sala deve ser preenchida!' });
  }

  if(Agenda.find( { data: new Date('2019-05-16')  })) {
    v_agenda_valida = false;    
    res.status(200).send({ message: 'Esta sala já está reservada para esta data!' });
  }
  
  if(v_agenda_valida)
  {
    agenda.save((err) => {
      if(!err) {
        console.log("Criado!");
        res.status(201)
        res.send(req.body)
      }
      else {
        console.log("Erro!");
        res.status(400)
        res.send(err)
        }
      });

  }

})

.delete('/agenda/:id', async (req, res) => {
  try {
      let id = req.params.id;
      if (id) {
          let data = await Agenda.findByIdAndRemove(id);
          if(data==null) {
            res.status(200).send({ message: 'Registro não encontrado!' });
          }
          else 
          {
            res.status(200).send({ message: 'Registro excluído com sucesso!' });
          }
      } else {
          res.status(400).send({ message: 'O parametro Id precisa ser informado.' });
      }
  } catch (error) {
      console.log('get com error, motivo: ', error);
      res.status(500).send({ message: 'Erro no processamento', error: error });
  }
});

const app = express()
.use(require('express').static('dist'))
.use('/api', api)
.get('*', (req, res) => {
  res.sendFile(require('path').join(__dirname, 'dist/index.html'));
})
.set('port', process.env.PORT)

const server = require('http').createServer(app)

server.listen(process.env.PORT, () =>
  console.log(`Aplicação iniciada na porta ${server.address().port}...`)
)
